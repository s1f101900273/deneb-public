module.exports = {
  extends: [
    "eslint:recommended",
    "plugin:import/typescript",
    "plugin:import/errors",
    "plugin:import/warnings",
    "prettier",
    "plugin:react/recommended",
    "plugin:react-hooks/recommended",
  ],
  plugins: ["unused-imports", "import"],
  rules: {
    "linebreak-style": ["error", "unix"],
    "react/no-unknown-property": ["error", { ignore: ["css"] }],
    "react/jsx-uses-react": "off",
    "react/react-in-jsx-scope": "off",
  },
  settings: {
    "import/resolver": {
      typescript: true,
      node: true,
    },
  },
  env: {
    browser: true,
    es6: true,
    node: true,
  },
};
