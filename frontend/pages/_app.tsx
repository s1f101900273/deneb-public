import { css, Global } from "@emotion/react";
import { AppProps } from "next/app";
import { MutableSnapshot, RecoilRoot } from "recoil";
import { Quiz } from "~/modules/data";
import { currentQuiz } from "~/modules/recoil";
import { deneb_background_cream } from "~/utils/color";

export default function App({
  Component,
  pageProps,
}: AppProps<{ quiz?: Quiz }>) {
  return (
    <>
      <RecoilRoot
        initializeState={(snapshot: MutableSnapshot) => {
          if (pageProps.quiz) {
            snapshot.set(currentQuiz, pageProps.quiz);
          }
        }}
      >
        <Component {...pageProps} />
      </RecoilRoot>
      <Global styles={globalStyle} />
    </>
  );
}

export const globalStyle = css`
  h1,
  h2,
  h3,
  h4,
  p {
    margin: 0;
  }

  body {
    margin: 0;
    font-family: "Noto Sans JP", Roboto, sans-serif;
    background-color: ${deneb_background_cream};
  }
`;
