import { ComponentMeta, ComponentStoryObj } from "@storybook/react";
import Layout from "~/components/layout/someitems";

type Story = ComponentStoryObj<typeof Layout>;
type Meta = ComponentMeta<typeof Layout>;

export default {
  parameters: {
    layout: "fullscreen",
  },
  component: Layout,
} as Meta;

export const WithSingle: Story = {
  args: {
    //isSingle
    id: "shopping-1",
    choices: ["選択肢1", "選択肢2", "選択肢3", "選択肢4"],
  },
};

export const WithMultiple: Story = {
  args: {
    //isMultiple
    id: "shopping-2",
    choices: ["選択肢1", "選択肢2", "選択肢3", "選択肢4"],
  },
};
