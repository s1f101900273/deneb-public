import { AnswerResult } from "~/modules/entity";
import isEqual from "lodash/isEqual";

export const checkAnswer = (answerResult: AnswerResult) => {
  if (answerResult.type === "single") {
    return answerResult.correctAnswer === answerResult.answer;
  } else {
    return isEqual(answerResult.correctAnswers, answerResult.answers);
  }
};
