import { DecoratorFn } from "@storybook/react";
import { RecoilRoot } from "recoil";

export const recoilDecorator: DecoratorFn = (Story) => (
  <RecoilRoot>
    <Story />
  </RecoilRoot>
);
