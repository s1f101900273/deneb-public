import { useEffect, useMemo, useState } from "react";
import { useRecoilValue } from "recoil";
import Template from "~/components/templates/question";
import {
  currentQuiz,
  isCompletedAnswersSelector,
  useResetAnswers,
} from "~/modules/recoil";
import { questionIDMapper, questions, Quiz, quiz } from "~/modules/data";
import Score from "~/components/templates/score";
import { GetServerSideProps, NextPage } from "next";

interface Props {
  quiz: Quiz;
}

export const getServerSideProps: GetServerSideProps<Props> = async ({
  query: { id },
}) => {
  if (!(typeof id === "string")) return { notFound: true };
  if (!quiz.find((val) => val === id)) return { notFound: true };
  return { props: { quiz: id as Quiz } };
};

const Index: NextPage<Props> = () => {
  const quiz = useRecoilValue(currentQuiz);
  const [showResult, setIsShowResult] = useState(false);
  const resetAnswer = useResetAnswers();

  const currentQuestions = useMemo(() => {
    if (!quiz) return [];
    /// getServerSidePropsでquizがQuizに変換できる文字列か判定している為
    return questionIDMapper[quiz as Quiz].map((id) => ({
      ...questions[id],
      id,
    }));
  }, [quiz]);

  useEffect(() => {
    resetAnswer(quiz);
  }, [resetAnswer, quiz]);

  const isEnabled = useRecoilValue(isCompletedAnswersSelector);
  return (
    <>
      {showResult ? (
        <>
          <Score
            onClickToTop={() => {
              resetAnswer(quiz);
              setIsShowResult(false);
            }}
          />
        </>
      ) : (
        <>
          {quiz && (
            <Template
              questions={currentQuestions}
              isDisabledNextButton={!isEnabled}
              onClick={() => setIsShowResult(true)}
            />
          )}
        </>
      )}
    </>
  );
};

export default Index;
