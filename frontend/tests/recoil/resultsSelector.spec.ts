import { vi } from "vitest";
import { Question } from "~/modules/entity";
import * as data from "~/modules/data";
import { snapshot_UNSTABLE } from "recoil";
import { answerState, currentQuiz, resultsSelector } from "~/modules/recoil";

describe("resultsSelector", () => {
  const dummySingleQuestion: Question = {
    title: "fuga",
    body: "piyo",
    choices: ["bar", "foo"],
    type: "single",
    correctAnswer: 0,
  };
  const dummyMultipleQuestion: Question = {
    title: "fuga",
    body: "piyo",
    choices: ["bar", "foo"],
    type: "multiple",
    correctAnswers: { 0: true, 1: false },
  };

  const dummySingleQuestionID = "xxxx";
  const dummyMultipleQuestionID = "yyyy";
  const dummyQuizID = "zzzzz";

  beforeEach(() => {
    vi.spyOn(data, "questions", "get").mockReturnValue({
      [dummySingleQuestionID]: dummySingleQuestion,
      [dummyMultipleQuestionID]: dummyMultipleQuestion,
    });
    vi.spyOn(data, "questionIDMapper", "get").mockReturnValue({
      [dummyQuizID]: [dummySingleQuestionID, dummyMultipleQuestionID],
    } as any);
  });

  afterEach(() => {
    vi.resetAllMocks();
  });

  it("should be success", async () => {
    const snapshot = snapshot_UNSTABLE(({ set }) => {
      // 初期化
      set(currentQuiz, dummyQuizID);

      // 値をセット
      set(answerState({ questionID: dummySingleQuestionID }), {
        type: "single",
        answer: 1,
      });
      set(answerState({ questionID: dummyMultipleQuestionID }), {
        type: "multiple",
        answers: {
          0: true,
          1: false,
        },
      });
    });

    const results = await snapshot.getPromise(resultsSelector);
    expect(results[0]).toStrictEqual({
      title: "fuga",
      body: "piyo",
      choices: ["bar", "foo"],
      type: "single",
      correctAnswer: 0,
      answer: 1,
    });
    expect(results[1]).toStrictEqual({
      title: "fuga",
      body: "piyo",
      choices: ["bar", "foo"],
      type: "multiple",
      correctAnswers: {
        0: true,
        1: false,
      },
      answers: {
        0: true,
        1: false,
      },
    });
  });
  it("有効な解答が入力されていない問題が存在する場合", async () => {
    const snapshot = snapshot_UNSTABLE(({ set }) => {
      //初期化
      set(currentQuiz, dummyQuizID);

      //dummySingleQuestionを初期値のままにする
      //値をセット
      set(answerState({ questionID: dummyMultipleQuestionID }), {
        type: "multiple",
        answers: {
          0: true,
          1: false,
        },
      });
    });

    const results = await snapshot.getPromise(resultsSelector);

    expect(results.length).toEqual(0);
  });
  it("問題と回答のタイプが異なる場合", async () => {
    const snapshot = snapshot_UNSTABLE(({ set }) => {
      //初期化
      set(currentQuiz, dummyQuizID);

      //dummySingleQuestionを初期値のままにする
      //値をセット
      set(answerState({ questionID: dummySingleQuestionID }), {
        type: "single",
        answer: 0,
      });
      set(answerState({ questionID: dummyMultipleQuestionID }), {
        type: "single",
        answer: 0,
      });
    });

    await expect(snapshot.getPromise(resultsSelector)).rejects.toThrow(
      "unknown error"
    );
  });
});
