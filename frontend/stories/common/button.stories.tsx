import { ComponentMeta, ComponentStoryObj } from "@storybook/react";
import Layout from "~/components/layout/button";

type Story = ComponentStoryObj<typeof Layout>;
type Meta = ComponentMeta<typeof Layout>;

export default {
  component: Layout,
} as Meta;

export const Default: Story = {
  args: {
    children: "OK",
  },
};

export const Disabled: Story = {
  args: {
    children: "Disabled",
    disable: true,
  },
};
