import { snapshot_UNSTABLE } from "recoil";
import {
  answerState,
  currentQuiz,
  isCompletedAnswersSelector,
} from "~/modules/recoil";
import { vi } from "vitest";
import * as data from "~/modules/data";
vi.doMock("~/modules/data");

const dummyQuizID = "yyyy";
const dummyQuestionID = "xxxx";
const dummySecondaryQuestionID = "ffff";

describe("isCompletedAnswersSelector", () => {
  beforeEach(() => {
    vi.spyOn<any, any>(data, "questionIDMapper", "get").mockReturnValue({
      [dummyQuizID]: [dummyQuestionID, dummySecondaryQuestionID],
    });
    vi.spyOn(data, "questions", "get").mockReturnValue({
      [dummyQuestionID]: {
        title: "aaaaa",
        body: "bbbbb",
        choices: ["1", "2", "3", "4"],
        type: "single",
        correctAnswer: 0,
      },
      [dummySecondaryQuestionID]: {
        title: "aaaaa",
        body: "bbbbb",
        choices: ["1", "2", "3"],
        type: "multiple",
        correctAnswers: {
          0: true,
          1: true,
          2: true,
        },
      },
    });
  });
  afterEach(() => {
    vi.resetAllMocks();
  });
  it("全ての問題に回答されている場合、trueを返す", async () => {
    const snapshot = snapshot_UNSTABLE(({ set }) => {
      set(currentQuiz, dummyQuizID);
      set(answerState({ questionID: dummyQuestionID }), {
        type: "single",
        answer: 0,
      });
      set(answerState({ questionID: dummySecondaryQuestionID }), {
        type: "multiple",
        answers: { 0: true, 1: false, 2: false },
      });
    });
    const result = await snapshot.getPromise(isCompletedAnswersSelector);
    expect(result).toEqual(true);
  });
  it("一問でも未入力の場合、falseを返す", async () => {
    const snapshot = snapshot_UNSTABLE(({ set }) => {
      set(currentQuiz, dummyQuizID);
      set(answerState({ questionID: dummyQuestionID }), {
        type: "single",
        answer: 0,
      });
    });
    const result = await snapshot.getPromise(isCompletedAnswersSelector);
    expect(result).toEqual(false);

    const otherSnapshot = snapshot_UNSTABLE(({ set }) => {
      set(currentQuiz, dummyQuizID);
      set(answerState({ questionID: dummySecondaryQuestionID }), {
        type: "multiple",
        answers: { 0: true, 1: false, 2: false },
      });
    });
    const otherResult = await otherSnapshot.getPromise(
      isCompletedAnswersSelector
    );
    expect(otherResult).toEqual(false);
  });
});
