import { FC } from "react";

type State = "empty" | "check" | "correct" | "incorrect";

interface Props {
  state: State;
}

const Accessory: FC<Props> = ({ state }) => (
  <svg
    width="20"
    height="20"
    viewBox="0 0 20 20"
    fill="none"
    xmlns="http://www.w3.org/2000/svg"
  >
    {(() => {
      switch (state) {
        case "empty":
          return (
            <circle
              cx="10"
              cy="10"
              r="6"
              stroke="#3FA1C0"
              strokeOpacity="0.2"
              strokeWidth="2"
            />
          );
        case "check":
          return <circle cx="10" cy="10" r="6" fill="#3FA1C0" />;
        case "correct":
          return (
            <circle cx="10" cy="10" r="8" stroke="#70C03F" strokeWidth="4" />
          );
        case "incorrect":
          return (
            <path
              fillRule="evenodd"
              clipRule="evenodd"
              d="M2.75069 2.75069C1.74977 3.75161 1.74977 5.37443 2.75069 6.37535L6.37535 10L2.75069 13.6247C1.74977 14.6256 1.74977 16.2484 2.75069 17.2493C3.75161 18.2502 5.37443 18.2502 6.37535 17.2493L10 13.6247L13.6247 17.2493C14.6256 18.2502 16.2484 18.2502 17.2493 17.2493C18.2502 16.2484 18.2502 14.6256 17.2493 13.6247L13.6247 10L17.2493 6.37535C18.2502 5.37443 18.2502 3.75161 17.2493 2.75069C16.2484 1.74977 14.6256 1.74977 13.6247 2.75069L10 6.37535L6.37535 2.75069C5.37443 1.74977 3.75161 1.74977 2.75069 2.75069Z"
              fill="#C0463F"
            />
          );
      }
    })()}
  </svg>
);

export default Accessory;
