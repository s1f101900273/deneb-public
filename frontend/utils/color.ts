import hexRgb from "hex-rgb";

export const deneb_background_cream = "#FCFBF7";

export const deneb_cyan = "#3FA1C0";

export const deneb_green = "#70C03F";

export const deneb_red = "#C0463F";

export const deneb_primary_black = "#0F0E2B";

export const deneb_secondary_black = "#1D0E2B";

export const deneb_clicked_white = "#F9F9F9";

export const withAlpha = (hex: string, alpha: number) =>
  hexRgb(hex, { alpha, format: "css" });
