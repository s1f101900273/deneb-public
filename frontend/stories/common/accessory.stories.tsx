import { ComponentMeta, ComponentStoryObj } from "@storybook/react";
import Accessory from "~/components/layout/accessory";

type Story = ComponentStoryObj<typeof Accessory>;
type Meta = ComponentMeta<typeof Accessory>;

export default {
  component: Accessory,
} as Meta;

export const Empty: Story = { args: { state: "empty" } };
export const Check: Story = { args: { state: "check" } };
export const Correct: Story = { args: { state: "correct" } };
export const InCorrect: Story = { args: { state: "incorrect" } };
