import { ComponentMeta, ComponentStoryObj } from "@storybook/react";
import Select from "~/components/layout/select";

type Story = ComponentStoryObj<typeof Select>;
type Meta = ComponentMeta<typeof Select>;

export default {
  component: Select,
} as Meta;

export const Empty: Story = {
  args: { state: "empty", text: "これはテストです" },
};
export const Checked: Story = {
  args: { state: "check", text: "これはテストです" },
};
export const Corrected: Story = {
  args: { state: "correct", text: "これはテストです" },
};
export const InCorrected: Story = {
  args: { state: "incorrect", text: "これはテストです" },
};
