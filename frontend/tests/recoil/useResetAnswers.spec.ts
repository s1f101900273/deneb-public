import { renderHook } from "@testing-library/react";
import { vi } from "vitest";
import { RecoilRoot, useRecoilValue, useSetRecoilState } from "recoil";
import * as data from "~/modules/data";
import { Answer, Question } from "~/modules/entity";
import { answerState, useResetAnswers } from "~/modules/recoil";
import { act } from "react-dom/test-utils";
vi.doMock("~/modules/data");

describe("useResetAnswers", () => {
  afterEach(() => {
    vi.resetAllMocks();
  });
  it("should be success", () => {
    const dummyQuizID = "xxxx";
    const dummyQuestionID = "yyyy";
    const dummySecondaryQuestionID = "zzzz";
    vi.spyOn<any, any>(data, "questionIDMapper", "get").mockReturnValue({
      [dummyQuizID]: [dummyQuestionID, dummySecondaryQuestionID],
    });
    vi.spyOn(data, "questions", "get").mockReturnValue({
      [dummyQuestionID]: {
        title: "aaaaa",
        body: "bbbbb",
        choices: ["1", "2", "3", "4"],
        type: "single",
        correctAnswer: 0,
      },
      [dummySecondaryQuestionID]: {
        title: "aaaaa",
        body: "bbbbb",
        choices: ["1", "2", "3"],
        type: "single",
        correctAnswer: 1,
      },
    });

    const { result } = renderHook(
      () => ({
        resetAnswers: useResetAnswers(),
        dummyQuestion: useRecoilValue(
          answerState({ questionID: dummyQuestionID })
        ),
        dummySecondaryQuestion: useRecoilValue(
          answerState({ questionID: dummySecondaryQuestionID })
        ),
        setDummyQuestion: useSetRecoilState(
          answerState({ questionID: dummyQuestionID })
        ),
        setDummySecondaryQuestion: useSetRecoilState(
          answerState({ questionID: dummySecondaryQuestionID })
        ),
      }),
      { wrapper: RecoilRoot }
    );

    act(() => {
      result.current.setDummyQuestion({
        type: "single",
        answer: 1,
      });
      result.current.setDummySecondaryQuestion({
        type: "single",
        answer: 0,
      });
    });
    expect(result.current.dummyQuestion).toStrictEqual({
      type: "single",
      answer: 1,
    });
    expect(result.current.dummySecondaryQuestion).toStrictEqual({
      type: "single",
      answer: 0,
    });

    act(() => {
      result.current.resetAnswers(dummyQuizID);
    });
    expect(result.current.dummyQuestion).toStrictEqual({
      type: "single",
      answer: -1,
    });
    expect(result.current.dummySecondaryQuestion).toStrictEqual({
      type: "single",
      answer: -1,
    });
  });
});
