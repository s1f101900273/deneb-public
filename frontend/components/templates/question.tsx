import QuestionItem from "~/components/layout/form";
import { css } from "@emotion/react";
import { Question } from "~/modules/entity";
import { FC } from "react";
import Button from "~/components/layout/button";

interface Props {
  questions: (Question & { id: string })[];
  isDisabledNextButton: boolean;
  onClick?: () => void;
}

const Index: FC<Props> = ({ questions, isDisabledNextButton, onClick }) => {
  return (
    <main css={mainStyle}>
      {questions.map((question, index) => (
        <QuestionItem index={index + 1} question={question} key={index} />
      ))}
      <div css={buttonContainerStyle}>
        <Button disable={isDisabledNextButton} onClick={onClick}>
          答え合わせ
        </Button>
      </div>
    </main>
  );
};

const mainStyle = css`
  display: grid;
  grid-template-columns: 1fr;
  gap: 20px;
  padding: 40px 0 80px;
`;

const buttonContainerStyle = css`
  display: flex;
  justify-content: center;
`;

export default Index;
