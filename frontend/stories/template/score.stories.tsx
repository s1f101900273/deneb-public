import { ComponentMeta, ComponentStoryObj } from "@storybook/react";
import Layout from "~/components/templates/score";

type Story = ComponentStoryObj<typeof Layout>;
type Meta = ComponentMeta<typeof Layout>;

export default {
  component: Layout,
  parameters: {
    layout: "fullscreen",
  },
} as Meta;

export const Choices: Story = {
  args: {},
};
