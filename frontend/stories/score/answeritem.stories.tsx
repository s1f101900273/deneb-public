import { ComponentMeta, ComponentStoryObj } from "@storybook/react";
import { screen, userEvent } from "@storybook/testing-library";
import Layout from "~/components/layout/score/answeritem";

type Story = ComponentStoryObj<typeof Layout>;
type Meta = ComponentMeta<typeof Layout>;

export default {
  component: Layout,
} as Meta;

export const SingleCorrected: Story = {
  args: {
    index: 1,
    answerResult: {
      type: "single",
      title: "ネットショッピング1",
      body: "出すぎここは眼をうるさいんと近くの勢の楽器らにき第一中みちのおじぎが云えながらいるたた。勢も前荒れがくださいます。ゴーシュは二ちがわトォテテテテテイのようにしています。お母さんは本気扉たりどこからすっていじ。",
      choices: ["セロ弾きのゴーシュ"],
      correctAnswer: 0,
      answer: 0,
    },
  },
  play: async () => {
    await userEvent.click(screen.getByRole("button"));
  },
};
export const SingleInCorrected: Story = {
  args: {
    index: 1,
    answerResult: {
      type: "single",
      title: "ネットショッピング1",
      body: "出すぎここは眼をうるさいんと近くの勢の楽器らにき第一中みちのおじぎが云えながらいるたた。勢も前荒れがくださいます。ゴーシュは二ちがわトォテテテテテイのようにしています。お母さんは本気扉たりどこからすっていじ。",
      choices: ["三匹の子豚", "セロ弾きのゴーシュ"],
      answer: 0,
      correctAnswer: 1,
    },
  },
  play: async () => {
    await userEvent.click(screen.getByRole("button"));
  },
};
export const MultipleCorrected: Story = {
  args: {
    index: 1,
    answerResult: {
      type: "multiple",
      title: "ネットショッピング1",
      body: "出すぎここは眼をうるさいんと近くの勢の楽器らにき第一中みちのおじぎが云えながらいるたた。勢も前荒れがくださいます。ゴーシュは二ちがわトォテテテテテイのようにしています。お母さんは本気扉たりどこからすっていじ。",
      choices: ["三匹の子豚", "セロ弾きのゴーシュ", "白雪姫"],
      correctAnswers: {
        0: true,
        1: true,
        2: true,
      },
      answers: {
        0: true,
        1: true,
        2: true,
      },
    },
  },
  play: async () => {
    await userEvent.click(screen.getByRole("button"));
  },
};
export const MultipleInCorrected: Story = {
  args: {
    index: 1,
    answerResult: {
      type: "multiple",
      title: "ネットショッピング1",
      body: "出すぎここは眼をうるさいんと近くの勢の楽器らにき第一中みちのおじぎが云えながらいるたた。勢も前荒れがくださいます。ゴーシュは二ちがわトォテテテテテイのようにしています。お母さんは本気扉たりどこからすっていじ。",
      choices: ["三匹の子豚", "セロ弾きのゴーシュ", "白雪姫"],
      correctAnswers: {
        0: true,
        1: false,
        2: true,
      },
      answers: {
        0: false,
        1: true,
        2: true,
      },
    },
  },
  play: async () => {
    await userEvent.click(screen.getByRole("button"));
  },
};
