import { css } from "@emotion/react";
import { FC } from "react";
import SomeItems from "~/components/layout/someitems";
import { Question } from "~/modules/entity";

interface Props {
  index: number;
  question: Question & { id: string };
}

const Form: FC<Props> = ({ index, question }) => (
  <div css={formContainerStyle}>
    <div css={textContainerStyle}>
      <h3 css={titleStyle}>
        {index}.{question.title}
      </h3>
      <div css={bodyStyle}>{question.body}</div>
    </div>
    <SomeItems {...question} />
  </div>
);

const formContainerStyle = css`
  display: flex;
  flex-direction: column;
  justify-content: center;
`;

const textContainerStyle = css`
  padding: 32px 30px 24px;
`;

const titleStyle = css`
  padding-bottom: 20px;
  text-align: center;
`;

const bodyStyle = css`
  font-size: 16px;
  line-height: 200%;
  white-space: pre-wrap;
`;

export default Form;
