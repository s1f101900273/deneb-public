import react from "@vitejs/plugin-react";
import path from "path";
import { defineConfig } from "vite";

// https://vitejs.dev/config/
export default defineConfig({
  plugins: [
    react({
      jsxImportSource: "@emotion/react",
      babel: {
        plugins: ["@emotion/babel-plugin"],
      },
    }),
  ],
  resolve: {
    alias: {
      "~/": path.join(__dirname, "./"),
    },
  },
  test: {
    globals: true,
    environment: "jsdom",
    setupFiles: ["./tests/helper/setup.ts"],
    coverage: {
      skipFull: true,
      reporter: ["cobertura", "text"],
    },
    reporters: ["junit", "verbose"],
    outputFile: {
      junit: "reporter/junit.xml",
    },
  },
});
