import { css } from "@emotion/react";
import { FC } from "react";
import Accessory from "~/components/layout/accessory";
import {
  deneb_clicked_white,
  deneb_cyan,
  deneb_green,
  deneb_primary_black,
  deneb_red,
  withAlpha,
} from "~/utils/color";

export type State = "empty" | "check" | "correct" | "incorrect";

interface Props {
  state: State;
  text: string;
  onClick: () => void;
}

const Select: FC<Props> = ({ state, text, onClick }) => (
  <button css={[itemStyle, currentStateStyle(state)]} onClick={onClick}>
    <span css={textStyle}>{text}</span>
    <Accessory state={state} />
  </button>
);

const itemGap = "8px";
const accessoryWidth = "20px";

const itemStyle = css`
  display: grid;

  grid-template-columns: 1fr ${accessoryWidth};
  gap: ${itemGap};
  align-items: center;
  align-self: center;
  justify-content: center;
  justify-self: stretch;
  width: 100%;

  padding: 14px 16px;

  font-size: 16px;
  text-align: center;

  user-select: none;

  background-color: white;

  border: solid 2px;
  border-radius: 8px;
  box-shadow: 2px 2px 4px rgba(0 0 0 25%);

  :active {
    background-color: ${deneb_clicked_white};
    box-shadow: none;
  }
`;

const unCheckedStyle = css`
  border-color: white;

  :active {
    border-color: ${deneb_clicked_white};
  }
`;

const checkedStyle = css`
  border-color: ${deneb_cyan};
`;

const correctStyle = css`
  border-color: ${deneb_green};
`;

const inCorrectStyle = css`
  border-color: ${deneb_red};
`;

const currentStateStyle = (state: State) => {
  switch (state) {
    case "empty":
      return unCheckedStyle;
    case "check":
      return checkedStyle;
    case "correct":
      return correctStyle;
    case "incorrect":
      return inCorrectStyle;
  }
};

const textStyle = css`
  padding-left: calc(${accessoryWidth} + ${itemGap});
  font-family: "Noto Sans JP", sans-serif;
  font-weight: 500;
  color: ${withAlpha(deneb_primary_black, 0.8)};
`;

export default Select;
