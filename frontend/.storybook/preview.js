import { globalStyleDecorator } from "~/decorators/globalStyleDecorator";
import { recoilDecorator } from "~/decorators/recoilDecorator";
import * as NextImage from "next/image";

export const parameters = {
  actions: { argTypesRegex: "^on[A-Z].*" },
  controls: {
    matchers: {
      color: /(background|color)$/i,
      date: /Date$/,
    },
  },
};

export const decorators = [recoilDecorator, globalStyleDecorator];
