module.exports = {
  extends: [
    "stylelint-config-standard",
    "stylelint-config-prettier",
    "stylelint-config-recess-order",
  ],
  ignoreFiles: ["node_modules/**"],
  overrides: [
    {
      files: ["**/*.ts", "**/*.tsx"],
      customSyntax: "@stylelint/postcss-css-in-js",
    },
  ],
  rules: {
    "value-keyword-case": null,
    "function-name-case": null,
    "declaration-empty-line-before": null,
    "function-no-unknown": null,
  },
};
