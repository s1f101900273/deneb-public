import { ComponentMeta, ComponentStoryObj } from "@storybook/react";
import Index from "~/components/templates/question";
import { questions } from "~/modules/data";

type Story = ComponentStoryObj<typeof Index>;
type Meta = ComponentMeta<typeof Index>;

export default {
  component: Index,
  parameters: {
    layout: "fullscreen",
  },
} as Meta;

export const Default: Story = {
  args: {
    questions: [
      { ...questions["phishing-1"], id: "phishing-1" },
      { ...questions["phishing-2"], id: "phishing-2" },
    ],
  },
};
