import {
  atom,
  atomFamily,
  selector,
  useRecoilCallback,
  waitForAll,
} from "recoil";
import { Answer, AnswerResult, AnswerWithID } from "./entity";
import isIncludes from "lodash.includes";
import { questionIDMapper, questions } from "./data";

const initialAnswerStateBuilder = (questionID: string): Answer => {
  const question = questions[questionID];
  if (question.type === "single") {
    return {
      type: question.type,
      answer: -1,
    };
  } else {
    return {
      type: question.type,
      answers: Object.fromEntries(
        [...Array(question.choices.length).keys()].map((index) => [
          index,
          false,
        ])
      ),
    };
  }
};

export const answerState = atomFamily<Answer, { questionID: string }>({
  key: "answer",
  default: ({ questionID }) => initialAnswerStateBuilder(questionID),
});

export const currentQuiz = atom<string | undefined>({
  key: "currentQuiz",
  default: undefined,
});

export const relatedAnswersSelector = selector<AnswerWithID[]>({
  key: "relatedAnswers",
  get: ({ get }) => {
    const quiz = get(currentQuiz);
    if (!quiz) return [];
    const mapper = questionIDMapper as { [key: string]: string[] };
    return get(
      waitForAll(mapper[quiz].map((id) => answerState({ questionID: id })))
    ).map((answer, index) => ({ ...answer, questionID: mapper[quiz][index] }));
  },
});

export const isCompletedAnswersSelector = selector<boolean>({
  key: "isCompletedAnswers",
  get: ({ get }) => {
    const relatedAnswers = get(relatedAnswersSelector);
    const isCompleted = relatedAnswers.map((answer) => {
      if (!answer) return false;
      switch (answer.type) {
        case "single":
          return answer.answer >= 0;
        case "multiple":
          return isIncludes(answer.answers, true);
      }
    });
    return !isIncludes(isCompleted, false);
  },
});

export const resultsSelector = selector<AnswerResult[]>({
  key: "result",
  get: ({ get }) => {
    const relatedAnswers = get(relatedAnswersSelector);
    if (
      relatedAnswers.find((answer) => {
        switch (answer.type) {
          case "multiple":
            return !Object.values(answer.answers).filter((val) => !!val);
          case "single":
            return answer.answer < 0;
        }
      })
    ) {
      return [];
    }
    return relatedAnswers.map((answer): AnswerResult => {
      const question = questions[answer.questionID];
      if (answer.type === "single" && question.type === "single") {
        return {
          ...question,
          answer: answer.answer,
        };
      } else if (answer.type === "multiple" && question.type === "multiple") {
        return {
          ...question,
          answers: answer.answers,
        };
      } else {
        throw "unknown error";
      }
    });
  },
});

export const useToggleSelectionAnswer = (questionID: string) => {
  return useRecoilCallback(
    ({ set }) =>
      async (index: number) => {
        set(answerState({ questionID }), (answer) => {
          if (!answer) return answer;
          switch (answer.type) {
            case "multiple":
              return {
                ...answer,
                answers: { ...answer.answers, [index]: !answer.answers[index] },
              };
            case "single":
              return {
                ...answer,
                answer: answer.answer !== index ? index : -1,
              };
          }
        });
      },
    []
  );
};

export const useResetAnswers = () => {
  return useRecoilCallback(
    ({ reset }) =>
      async (id?: string) => {
        if (!id) return;
        const mapper = questionIDMapper as { [key: string]: string[] };
        const questionIDs = mapper[id];
        questionIDs.forEach((id) => {
          reset(answerState({ questionID: id }));
        });
      },
    []
  );
};
