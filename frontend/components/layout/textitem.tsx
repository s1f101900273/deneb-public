import { css } from "@emotion/react";

const TextItem = () => (
  <div css={containerStyle}>
    <h3>解答</h3>
    <input type="text" defaultValue="解答を入力"></input>
  </div>
);

const containerStyle = css`
  input {
    width: 80%;
    height: 30px;
  }
`;

export default TextItem;
