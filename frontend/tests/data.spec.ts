import { questionIDMapper, questions } from "~/modules/data";

const questionIDs = Object.keys(questions);

describe("questionIDMappper", () => {
  describe("存在するquestionIDが設定されているか", () => {
    describe.each(
      Object.keys(questionIDMapper).map((quiz): [string, string[]] => [
        quiz,
        (questionIDMapper as { [key: string]: string[] })[quiz],
      ])
    )("%s", (_, ids) => {
      it.each(ids)("%s", (id) => {
        expect(questionIDs.includes(id)).toEqual(true);
      });
    });
  });
});
