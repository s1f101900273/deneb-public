import { Question } from "./entity";

export const quiz = [
  "phishing",
  "shopping",
  "fake",
  "leak",
  "feedback",
] as const;

export type Quiz = typeof quiz[number];

export const questions: Record<string, Question> = {
  "phishing-1": {
    title: "フィッシング1",
    body: "【メールの件名】\n【重要】楽●株式会社から緊急のご連絡\n\n【サイトのURL例】\n • メール内のURL:\n http://redirect-•••-raku••.com/ \n • 転送先のURL:\n http://account-recovery-•••-raku••.com/ \n\n 突然まちこのもとにこのようなメールが届きました。このアドレスからメールが届くのは初めてです。あなたはこの会社のサービスをよく利用するユーザーです。まちこはこの後どんな行動をとるべきですか?",
    choices: [
      "心配なのでURLをクリック",
      "自分でHPなどを開きお知らせをチェック",
      "無視する",
    ],
    type: "single",
    correctAnswer: 1,
  },
  "phishing-2": {
    title: "フィッシング2",
    body: "まちこはメールを信頼し、前のメールのURLをクリックしてしまいました。この後まちこはどうするのが良いですか？",
    choices: [
      "指示に従って必要な項目を入力",
      "サイトに表示されているサポートセンターに電話",
      "落ち着いて無視をする",
    ],
    type: "single",
    correctAnswer: 2,
  },
  "phishing-3": {
    title: "フィッシング3",
    body: "まちこは不審に思いサイトに表示されている番号に電話をかけたところ、「会社のセキュリティが変わったのでもう一度必要な項目を入力して欲しい。」と言われました。\n「また手続きをしなかった場合5万円が請求される」という趣旨も説明されました。まちこはこの後どのような行動を取るべきですか?",
    choices: [
      "指示に従って必要な項目を入力",
      "怒って苦情の電話を入れる",
      "落ち着いて無視をする",
    ],
    type: "single",
    correctAnswer: 2,
  },
  "phishing-4": {
    title: "フィッシング4",
    body: "まちこは友達に相談をし、電話の内容を無視することにしました。しかし、電話はしつこくかかってきて、「必要な項目を入力しなければ５万円の請求が発生します。」と強い口調で脅されます。まちこは不安でいっぱいです。まちこはこの後どのような行動を取るべきでしょうか?",
    choices: [
      "怖いので指示に従って必要な項目を入力",
      "警察に電話をする",
      "落ち着いて無視をする",
    ],
    type: "single",
    correctAnswer: 1,
  },
  "shopping-1": {
    title: "ネットショッピング1",
    body: "まちこさんはネットショッピングでヘッドホンを注文しました。しかし、数日経っても商品が届きません。まちこさんは詐欺にあったと思いました。まちこさんが最初に取るべき行動を選択しなさい。",
    choices: [
      "気長に待つ",
      "国民生活センターに連絡",
      "本当に詐欺サイトか確認",
      "カード会社に連絡",
    ],
    type: "single",
    correctAnswer: 1,
  },
  "shopping-2": {
    title: "ネットショッピング2",
    body: "まちこさんはあるサイトでブランドバッグを注文しました。しかし、明らかに写真と違う商品が届きました。ネットショッピングで商品を購入する際に注意すべき点を選択しなさい。（複数選択可）",
    choices: [
      "URLの表示、字体",
      "振込先",
      "販売先の住所、連絡先",
      "どれくらい値引きされているか",
    ],
    type: "multiple",
    correctAnswers: {
      0: true,
      1: true,
      2: true,
      3: true,
    },
  },
  "shopping-3": {
    title: "ネットショッピング3",
    body: "まちこさんはあるサイトで野菜ジュースを初回お試し価格で購入することにした。しかし、まちこさんは多額の請求を受けてしまった。まちこさんが詐欺被害にあわないために購入の際に最も注意するべきだったことを選択しなさい。",
    choices: [
      "原材料のチェック",
      "野菜ジュースの販売会社",
      "商品のレビュー",
      "契約内容",
    ],
    type: "single",
    correctAnswer: 3,
  },
  "fake-1": {
    title: "偽情報1",
    body: "まちこはSNSで「知り合いの地質学者からの情報で3日後に大きな地震が来るから食べ物などを買って備えた方が良い。」というつぶやきを目にしました。\nこのつぶやきは既に多くの拡散がされています。多くの人が信じているようなのでまちこも信じるべきでしょうか?",
    choices: [
      "信じる",
      "無視する",
      "新聞記事やニュースなどで似たような情報があるか確認する",
    ],
    type: "single",
    correctAnswer: 2,
  },
  "fake-2": {
    title: "偽情報2",
    body: "まちこは、そのつぶやきを親友も拡散していたので信じることにしました。しかし、テレビや新聞で同じようなニュースは目にしません。まちこはこの後をどうするべきでしょうか?",
    choices: ["拡散する", "無視する", "家族に地震のことを警告する"],
    type: "single",
    correctAnswer: 1,
  },
  "leak-1": {
    title: "情報漏洩1",
    body: "まちこが自宅で撮影した自撮り写真をSNSに投稿しました。すると、待ち伏せやストーカーの被害にあってしまいました。\n自撮り写真などをSNSへ投稿する際に注意すべき点を選択しなさい。（複数選択可)",
    choices: [
      "自宅周辺で撮影した写真の投稿",
      "自分の瞳に景色が映っている写真の投稿",
      "部屋の間取りが分かる写真の投稿",
    ],
    type: "multiple",
    correctAnswers: {
      0: true,
      1: true,
      2: true,
    },
  },
  "leak-2": {
    title: "情報漏洩2",
    body: "まちこはiPhoneユーザーであり頻繫にAirDrop機能で写真などを友人と共有しています。\nある日、AirDrop機能で見知らぬ人から写真のファイルが送られてきました。まちこはどのような行動をすべきですか？",
    choices: [
      "写真ファイルを開く",
      "写真ファイルを開かない",
      "近くに友達がいないか確認",
    ],
    type: "single",
    correctAnswer: 1,
  },
  "leak-3": {
    title: "情報漏洩3",
    body: "まちこは写真ファイルを開いてしまいました。この後、まちこはどうするべきですか？",
    choices: ["写真ファイルを削除", "写真ファイルを友達にも共有", "放置する"],
    type: "single",
    correctAnswer: 0,
  },
  "leak-4": {
    title: "情報漏洩4",
    body: "まちこは写真ファイルを削除しました。この後、まちこはどうするべきですか？（複数選択可)",
    choices: [
      "AirDrop機能をoffにする",
      "AirDrop機能を連絡先を持っている人に限定する",
      "iPhoneの名前を変更する",
    ],
    type: "multiple",
    correctAnswers: {
      0: true,
      1: true,
      2: true,
    },
  },
};

export const questionIDMapper: Record<Quiz, string[]> = {
  phishing: ["phishing-1", "phishing-2", "phishing-3", "phishing-4"],
  shopping: ["shopping-1", "shopping-2", "shopping-3"],
  fake: ["fake-1", "fake-2"],
  leak: ["leak-1", "leak-2", "leak-3", "leak-4"],
  feedback: [
    "fake-1",
    "fake-2",
    "phishing-1",
    "phishing-2",
    "phishing-3",
    "phishing-4",
    "shopping-1",
    "shopping-2",
    "shopping-3",
    "leak-1",
  ],
};
