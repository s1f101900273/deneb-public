import { ComponentMeta, ComponentStoryObj } from "@storybook/react";
import Layout from "~/components/layout/form";

type Story = ComponentStoryObj<typeof Layout>;
type Meta = ComponentMeta<typeof Layout>;

export default {
  component: Layout,
  parameters: {
    layout: "fullscreen",
  },
} as Meta;

export const Choices: Story = {
  args: {
    index: 1,
    question: {
      id: "phishing-1",
      title: "ネットショッピング",
      body: "Aさんはネットショッピングでヘッドホンを注文しました。しかし、数日経っても商品が届きません。Aさんは詐欺にあったと思いました。Aさんが最初に取るべき行動を選択しなさい。",
      choices: ["選択肢1", "選択肢2", "選択肢3", "選択肢4"],
      type: "single",
      correctAnswer: 1,
    },
  },
};
