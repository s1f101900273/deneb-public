import { renderHook } from "@testing-library/react";
import { vi } from "vitest";
import { RecoilRoot, useRecoilValue, useSetRecoilState } from "recoil";
import * as data from "~/modules/data";
import { Answer, Question } from "~/modules/entity";
import { answerState, useToggleSelectionAnswer } from "~/modules/recoil";
import { act } from "react-dom/test-utils";
vi.doMock("~/modules/data");

describe("useToggleSelectionAnswer", () => {
  afterEach(() => {
    vi.resetAllMocks();
  });
  const cases: [string, Partial<Question>, Answer, number, Answer][] = [
    [
      "single",
      { type: "single" },
      { type: "single", answer: -1 },
      1,
      { type: "single", answer: 1 },
    ],
    [
      "single",
      { type: "single" },
      { type: "single", answer: 1 },
      1,
      { type: "single", answer: -1 },
    ],
    [
      "multiple",
      { type: "multiple", choices: ["xxxxx", "xxxxx", "xxxxx"] },
      { type: "multiple", answers: { 0: false, 1: false, 2: true } },
      0,
      { type: "multiple", answers: { 0: true, 1: false, 2: true } },
    ],
    [
      "multiple",
      { type: "multiple", choices: ["xxxxx", "xxxxx", "xxxxx"] },
      { type: "multiple", answers: { 0: true, 1: false, 2: true } },
      0,
      { type: "multiple", answers: { 0: false, 1: false, 2: true } },
    ],
  ];
  it.each(cases)(
    "typeが%sの場合",
    (_, question, initialAnswer, target, correctAnswer) => {
      const id = "xxxx";
      vi.spyOn(data, "questions", "get").mockReturnValue({
        [id]: question as Question,
      });
      const { result } = renderHook(
        () => ({
          toggle: useToggleSelectionAnswer(id),
          answer: useRecoilValue(answerState({ questionID: id })),
          setAnswer: useSetRecoilState(answerState({ questionID: id })),
        }),
        {
          wrapper: RecoilRoot,
        }
      );
      act(() => {
        // 初期化
        result.current.setAnswer(initialAnswer);
      });
      expect(result.current.answer).toStrictEqual(initialAnswer);
      act(() => {
        result.current.toggle(target);
      });
      expect(result.current.answer).toStrictEqual(correctAnswer);
    }
  );
});
