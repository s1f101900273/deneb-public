import { css } from "@emotion/react";
import { FC, useMemo } from "react";
import { deneb_background_cream } from "~/utils/color";
import Select, { State } from "~/components/layout/select";
import { useRecoilValue } from "recoil";
import { answerState, useToggleSelectionAnswer } from "~/modules/recoil";
import { Answer } from "~/modules/entity";

interface Props {
  id: string;
  choices: string[];
}

const isSelected = (current: Answer | undefined, target: number) => {
  if (!current) return false;
  switch (current.type) {
    case "multiple":
      return current.answers[target];
    case "single":
      return current.answer === target;
  }
};

type Item = {
  state: State;
  text: string;
};

const SomeItems: FC<Props> = ({ id, choices }) => {
  const toggleAnser = useToggleSelectionAnswer(id);
  const currentAnswer = useRecoilValue(answerState({ questionID: id }));
  const items = useMemo(
    () =>
      choices.map(
        (item, index): Item => ({
          text: item,
          state: isSelected(currentAnswer, index) ? "check" : "empty",
        })
      ),
    // eslint-disable-next-line react-hooks/exhaustive-deps
    [currentAnswer]
  );

  return (
    <div css={itemContainerStyle}>
      {items.map((item, index) => (
        <Select {...item} key={index} onClick={() => toggleAnser(index)} />
      ))}
    </div>
  );
};
const itemContainerStyle = css`
  display: grid;
  grid-template-columns: 1fr;
  gap: 10px;
  padding: 20px 32px 44px;
  background-color: ${deneb_background_cream};
`;

export default SomeItems;
