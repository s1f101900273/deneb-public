import { Global } from "@emotion/react";
import { DecoratorFn } from "@storybook/react";
import { Fragment } from "react";
import { globalStyle } from "~/pages/_app";

export const globalStyleDecorator: DecoratorFn = (Story) => (
  <Fragment>
    <Story />
    <Global styles={globalStyle} />
  </Fragment>
);
