import { css } from "@emotion/react";
import { FC, useEffect, useMemo } from "react";
import { deneb_primary_black } from "~/utils/color";
import AnswerItem from "~/components/layout/score/answeritem";
import Button from "~/components/layout/button";
import { useRecoilValue } from "recoil";
import { resultsSelector } from "~/modules/recoil";
import { checkAnswer } from "~/utils/checkAnswer";

interface Props {
  onClickToTop?: () => void;
}

const Score: FC<Props> = ({ onClickToTop }) => {
  const results = useRecoilValue(resultsSelector);
  const score = useMemo(
    () => results.filter((result) => checkAnswer(result)).length,
    [results]
  );
  useEffect(() => {
    window.scrollTo(0, 0);
  }, []);
  return (
    <main css={containerStyle}>
      <h2 css={titleStyle}>
        {results.length}問中{score}問正解しました🎉
      </h2>
      <div css={resultListStyle}>
        {results.map((result, index) => (
          <AnswerItem key={index} index={index} answerResult={result} />
        ))}
      </div>
      <div css={buttonContainerStyle}>
        <Button onClick={onClickToTop}>トップへ戻る</Button>
      </div>
    </main>
  );
};

const containerStyle = css`
  padding: 0 20px;
`;

const titleStyle = css`
  padding-top: 72px;
  padding-bottom: 72px;
  font-size: 20px;
  font-weight: 700;
  color: ${deneb_primary_black};
  text-align: center;
`;

const resultListStyle = css`
  display: grid;
  grid-template-columns: 100%;
  gap: 16px;
`;

const buttonContainerStyle = css`
  display: flex;
  justify-content: center;
  width: 100%;

  padding-top: 36px;
  padding-bottom: 60px;
`;

export default Score;
