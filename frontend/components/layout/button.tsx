import { css } from "@emotion/react";
import { FC, PropsWithChildren } from "react";
import {
  deneb_clicked_white,
  deneb_secondary_black,
  withAlpha,
} from "~/utils/color";

interface Props {
  disable?: boolean;
  onClick?: () => void;
}

const Button: FC<PropsWithChildren<Props>> = ({
  children,
  disable = false,
  onClick,
}) => (
  <button css={buttonStyle} disabled={disable} onClick={onClick}>
    {children}
  </button>
);

const buttonStyle = css`
  box-sizing: border-box;
  min-width: 220px;
  padding: 8px;
  margin: 0;

  font-size: 20px;
  font-weight: 700;

  color: ${deneb_secondary_black};

  user-select: none;
  background-color: white;
  border: solid 2px ${deneb_secondary_black};
  border-radius: 8px;
  box-shadow: 2px 2px 4px rgb(0 0 0 / 25%);

  :not(:disabled):active {
    background-color: ${deneb_clicked_white};
    box-shadow: none;
  }

  :disabled {
    color: ${withAlpha(deneb_secondary_black, 0.48)};
    border-color: ${withAlpha(deneb_secondary_black, 0.48)};
    box-shadow: none;
  }
`;

export default Button;
