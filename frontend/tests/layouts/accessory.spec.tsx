import { composeStory } from "@storybook/react";
import { render } from "@testing-library/react";
import * as stories from "~/stories/common/accessory.stories";

const cases: [stories: keyof typeof stories][] = [
  ["Empty"],
  ["Check"],
  ["Correct"],
  ["InCorrect"],
];

describe("Accessory component", () => {
  describe.each(cases)("%s", (story) => {
    const Story = composeStory(stories[story], stories.default);

    it("render component", () => {
      const { container } = render(<Story />);

      expect(container.firstChild).toMatchSnapshot();
    });
  });
});
