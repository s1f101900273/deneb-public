export type QuestionContent = {
  type: "choice";
  isMultiple: boolean;
  choices: string[];
};

export type Question = { title: string; body: string; choices: string[] } & (
  | {
      type: "single";
      correctAnswer: number;
    }
  | {
      type: "multiple";
      correctAnswers: { [key: number]: boolean };
    }
);

export type Answer =
  | {
      type: "single";
      answer: number;
    }
  | {
      type: "multiple";
      answers: { [key: number]: boolean };
    };

export type AnswerWithID = Answer & { questionID: string };

export interface CurrentState {
  visible?: {
    id: string;
    pageState: "question" | "answer";
  };
}

export type AnswerResult = {
  title: string;
  body: string;
  choices: string[];
} & (
  | {
      type: "single";
      correctAnswer: number;
      answer: number;
    }
  | {
      type: "multiple";
      correctAnswers: { [key: number]: boolean };
      answers: { [key: number]: boolean };
    }
);
