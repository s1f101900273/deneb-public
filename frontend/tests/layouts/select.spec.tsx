import { composeStory } from "@storybook/react";
import { render } from "@testing-library/react";
import * as stories from "~/stories/select.stories";

const cases: [stories: keyof typeof stories][] = [
  ["Empty"],
  ["Checked"],
  ["Corrected"],
  ["InCorrected"],
];

describe("Select component", () => {
  describe.each(cases)("%s", (story) => {
    const Story = composeStory(stories[story], stories.default);

    it("render component", () => {
      const { container } = render(<Story />);

      expect(container.firstChild).toMatchSnapshot();
    });
  });
});
