import { css } from "@emotion/react";
import hexRgb from "hex-rgb";
import { FC, useEffect, useMemo, useState } from "react";
import {
  deneb_clicked_white,
  deneb_primary_black,
  withAlpha,
} from "~/utils/color";
import correctCircle from "~/public/correct_circle.svg";
import inCorrectCross from "~/public/incorrect_cross.svg";
import { AnswerResult } from "~/modules/entity";
import { checkAnswer } from "~/utils/checkAnswer";

interface Props {
  answerResult: AnswerResult;
  index: number;
}

const answersIndexes = (answer: { [key: number]: boolean }) =>
  Object.keys(answer)
    .filter((index) => answer[parseInt(index)])
    .map((answer) => parseInt(answer) + 1)
    .sort()
    .join(", ");

const AnswerItem: FC<Props> = ({ answerResult, index }) => {
  const [isOpen, setIsOpen] = useState(false);
  const isCorrect = useMemo(() => checkAnswer(answerResult), [answerResult]);
  useEffect(() => {
    if (index === 0) {
      setIsOpen(true);
    }
  }, [index]);
  return (
    <details css={detailsStyle} open={isOpen}>
      <summary css={summaryStyle}>
        <button
          css={itemStyle}
          onClick={() => setIsOpen((current) => !current)}
        >
          <p>
            <span css={countStyle}>{index + 1}.</span>
            <span css={titleStyle}>{answerResult.title}</span>
          </p>
          {/* eslint-disable-next-line @next/next/no-img-element */}
          <img
            src={isCorrect ? correctCircle : inCorrectCross}
            alt={""}
            width={20}
            height={20}
          />
        </button>
      </summary>
      <div css={infoStyle}>
        <div css={quizTextStyle}>{answerResult.body}</div>
        {answerResult.type === "single" && (
          <>
            <p css={correctAnswerSectionTitleStyle}>答え</p>
            <p css={correctAnswerTextStyle}>
              {answerResult.choices[answerResult.correctAnswer]}
            </p>
            {!isCorrect && (
              <p css={answerTextStyle}>
                あなたの解答: {answerResult.choices[answerResult.answer]}
              </p>
            )}
          </>
        )}
        {answerResult.type === "multiple" && (
          <>
            <div css={choicesContainerStyle}>
              <p css={choicesSectionTitleStyle}>選択肢</p>
              <ul css={listStyle}>
                {answerResult.choices.map((choice, index) => (
                  <li css={listItemStyle} key={index}>
                    {index + 1}. {choice}
                  </li>
                ))}
              </ul>
            </div>
            <p css={correctAnswerSectionTitleStyle}>答え</p>
            <p css={correctAnswerTextStyle}>
              {answersIndexes(answerResult.correctAnswers)}
            </p>
            {!isCorrect && (
              <>
                <p css={answerSectionTitleStyle}>あなたの答え</p>
                <p css={answerTextStyle}>
                  {answersIndexes(answerResult.answers)}
                </p>
              </>
            )}
          </>
        )}
      </div>
    </details>
  );
};

const detailsStyle = css`
  user-select: none;
  background-color: white;
  border-radius: 4px;
  box-shadow: 2px 2px 4px rgb(0 0 0 / 25%);
`;

const summaryStyle = css`
  ::marker {
    display: none;
    content: "";
  }

  ::-webkit-details-marker {
    display: none;
  }
`;

const itemStyle = css`
  display: flex;
  align-items: center;
  align-self: center;
  justify-content: space-between;
  justify-self: stretch;
  width: 100%;

  height: 40px;
  padding: 0 16px;
  color: ${hexRgb(deneb_primary_black, { alpha: 0.8, format: "css" })};

  text-align: center;

  user-select: none;
  background-color: inherit;
  border: none;
  border-radius: 4px;

  :active {
    background-color: ${deneb_clicked_white};
  }
`;

const infoStyle = css`
  display: block;
  padding: 20px;
`;

const countStyle = css`
  padding-right: 4px;
  font-size: 16px;
  font-weight: 300;
  color: ${withAlpha(deneb_primary_black, 0.8)};
`;

const titleStyle = css`
  font-size: 16px;
  font-weight: 300;
  color: ${withAlpha(deneb_primary_black, 0.8)};
  text-align: left;
`;

const quizTextStyle = css`
  font-size: 12px;
  font-weight: 400;
  line-height: 200%;
  color: ${withAlpha(deneb_primary_black, 0.6)};
  white-space: pre-wrap;
`;

const choicesContainerStyle = css`
  padding: 12px;
`;

const choicesSectionTitleStyle = css`
  font-size: 12px;
  font-weight: 500;
  color: ${withAlpha(deneb_primary_black, 0.85)};
`;

const listStyle = css`
  padding: 14px 0 0;
  margin: 0;
  list-style-type: none;
`;

const listItemStyle = css`
  padding-left: 4px;
  font-size: 12px;
  font-weight: 400;
  color: ${withAlpha(deneb_primary_black, 0.85)};

  :not(:last-child) {
    margin-bottom: 14px;
  }
`;

const correctAnswerSectionTitleStyle = css`
  width: 100%;

  padding-top: 16px;
  font-size: 12px;
  font-weight: 500;
  color: ${withAlpha(deneb_primary_black, 0.85)};
  text-align: center;
`;

const correctAnswerTextStyle = css`
  width: 100%;

  padding: 8px 0;
  font-size: 16px;
  font-weight: 700;
  color: ${withAlpha(deneb_primary_black, 0.9)};
  text-align: center;
`;

const answerSectionTitleStyle = css`
  width: 100%;

  padding: 8px 0;
  font-size: 12px;
  font-weight: 400;
  color: ${withAlpha(deneb_primary_black, 0.85)};
  text-align: center;
`;

const answerTextStyle = css`
  width: 100%;

  padding-bottom: 8px;
  font-size: 14px;
  font-weight: 400;
  color: ${withAlpha(deneb_primary_black, 0.85)};
  text-align: center;
`;

export default AnswerItem;
