// deno-lint-ignore-file

import "@testing-library/jest-dom";
import { createSerializer } from "@emotion/jest";
import { setGlobalConfig } from "@storybook/testing-react";
import { expect } from "vitest";
import * as globalStorybookConfig from "../../.storybook/preview";

setGlobalConfig(globalStorybookConfig);
expect.addSnapshotSerializer(createSerializer() as any);
