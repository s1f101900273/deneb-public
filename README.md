# Deneb

## Setup

install all dependencies
```shell
yarn install
```

### VSCode Plugins

- [dprint](https://marketplace.visualstudio.com/items?itemName=dprint.dprint)
- [vscode-styled-components](https://marketplace.visualstudio.com/items?itemName=styled-components.vscode-styled-components)
- [code-spell-checker](https://marketplace.visualstudio.com/items?itemName=streetsidesoftware.code-spell-checker)

## Usage

### frontend

#### Build

```shell
yarn workspace @deneb/frontend build

or 

yarn build
```

#### Debug run

```shell
yarn workspace @deneb/frontend dev

or 

yarn dev
```

#### Storybook

```shell
yarn workspace @deneb/frontend storybook
```

#### Test

```shell
yarn workspace @deneb/frontend test ## -u option to update snapshot tests
```
